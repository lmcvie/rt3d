#ifndef LABEL_H
#define LABEL_H

#include <SDL_ttf.h>
#include <GL/glew.h>
#include <ctime>
#include <iostream>

class Label
{
	public:
		static void exitFatalError(char *message);
		void textToTexture(TTF_Font* textFont, const char * str);
		void draw(float x, float y); 
	private:
		GLuint texID;
		GLuint height;
		GLuint width;
};
#endif