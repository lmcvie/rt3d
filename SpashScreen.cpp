#include "SplashScreen.h"
#include "game.h"


void SplashScreen::init(Game& context) {
	label = new Label();
	std::srand( std::time(NULL) );
	lastTime = clock();
}

bool SplashScreen::handleSDLEvent(SDL_Event const &sdlEvent, Game &context) {
	bool running = true;


	if (sdlEvent.type == SDL_KEYDOWN)
	{
		if (sdlEvent.key.keysym.sym == SDLK_RETURN)
		{
			context.setState(context.getMainMenuState());
			running = true;
		}
		if (sdlEvent.key.keysym.sym == SDLK_SPACE)
		{
			context.setState(context.getMainMenuState());
			running = true;
		}
		if (sdlEvent.key.keysym.sym == SDLK_ESCAPE)
		{
			running = false;
			SDL_DestroyWindow(context.window);
			SDL_Quit();
		}
	return running;
}
}

void SplashScreen::draw(SDL_Window* window, Game& context) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	label->textToTexture(textFont,"The Created by Venomous Gaming Company ");
	label->draw(-0.3,0.0);
	
	currentTime = clock()/1000;

	if((currentTime)>=3){
		context.setState(context.getMainMenuState());
	}

	SDL_GL_SwapWindow(window);
	 
}