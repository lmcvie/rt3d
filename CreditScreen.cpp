#include "CreditScreen.h"
#include "game.h"

void CreditScreen::init(Game& context) {
	label = new Label();
	std::srand( std::time(NULL) );
	lastTime = clock();
}

bool CreditScreen::handleSDLEvent(SDL_Event const &sdlEvent, Game &context) {
	bool running = true;
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		if (sdlEvent.key.keysym.sym == SDLK_RETURN)
		{
			context.setState(context.getMainMenuState());
			running = true;
		}
		if (sdlEvent.key.keysym.sym == SDLK_ESCAPE)
		{
			running = true;
			context.setState(context.getMainMenuState());
		}
	return running;
}
}

void CreditScreen::draw(SDL_Window* window, Game& context) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	label->textToTexture(textFont,"Welcome To The Galactic Marine Prototype");
	label->draw(-0.4,0.2);
	label->textToTexture(textFont,"developed by Venemous Gaming");
	label->draw(-0.4,0.0);
	label->textToTexture(textFont,"and completed on 18/12/12");
	label->draw(-0.4,-0.2);
	SDL_GL_SwapWindow(window);
	 

	currentTime = clock()/1000;

	if((currentTime)>=30){
		context.setState(context.getMainMenuState());
	}
}