#include "CombatScreen.h"
#include "game.h"

using namespace std;

extern int playerStats[4];
extern int brute, fodder, raider;


void CombatScreen::init(Game& context) {
	label = new Label();
	healthlost = 0;
	ifstream fodFile("fodder.txt", ios::in);
	i = 0;
	while(!fodFile.eof()){ 
		string s;
		getline(fodFile, s);
		fodderStats[i]=atoi(s.c_str()); 
		i++;
	}
	fodFile.close();

	ifstream bruFile("brute.txt", ios::in);
	i = 0;
	while(!bruFile.eof()){ 
		string s;
		getline(bruFile, s);
		bruteStats[i]=atoi(s.c_str()); 
		i++;
	}
	bruFile.close();

	ifstream raiFile("raider.txt", ios::in);
	i = 0;
	while(!raiFile.eof()){ 
		string s;
		getline(raiFile, s);
		raiderStats[i]=atoi(s.c_str()); 
		i++;
	}
	raiFile.close();
}

bool CombatScreen::handleSDLEvent(SDL_Event const &sdlEvent, Game &context) {
	extern int type;
	bool running = true;
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		if (sdlEvent.key.keysym.sym == SDLK_RETURN)
		{
			if (type== 1){
				playerStats[3]= playerStats[3]+1;
				fodderStats[0]=3;
			}
			if (type== 2){
				playerStats[3]= playerStats[3]+10;
				bruteStats[0]=12;
			}
			if (type== 3){
				playerStats[3]= playerStats[3]+100;
				raiderStats[0]=12;
			}
			playerStats[0]=(playerStats[0]+(healthlost/2));
			healthlost=0;
			context.setState(context.getPlayState());

			running = true;
		}		
		if (sdlEvent.key.keysym.sym == SDLK_ESCAPE)
		{
			context.setState(context.getMainMenuState());
			running = true;
		}
		return running;
	}
}


void CombatScreen::draw(SDL_Window* window, Game& context) {
	extern int type;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	label->textToTexture(textFont,"COMBAT SCREEN");
	label->draw(-0.1,0.5);
	glColor3f(1.0,1.0,1.0);
	glBegin(GL_POLYGON);
	glVertex3f (-0.6, 0, 0.0); // first corner
	glVertex3f (-0.4, 0, 0.0); // second corner
	glVertex3f (-0.4, 0.2, 0.0); // third corner
	glVertex3f (-0.6, 0.2, 0.0); // fourth corner
	glEnd();

	label->textToTexture(textFont, "Player"); // placing name above player
	label->draw(-0.6, 0.4);

	glColor3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);
	glVertex3f (0.6,0, 0.0); // first corner
	glVertex3f (0.4,0, 0.0); // second corner
	glVertex3f (0.4,0.2, 0.0); // third corner
	glVertex3f (0.6,0.2, 0.0); // fourth corner
	glEnd();

	std::stringstream strStream1;
	std::stringstream strStream2;

	strStream1 << "Health:  " << playerStats[0] << " Strength:  " << playerStats[1] << " Speed:  " << playerStats[2] << " Winnings:  " << playerStats[3];
	label->textToTexture(textFont,strStream1.str().c_str());
	label->draw(-0.99,-0.9);


	//fodder fight
	if (type== 1){
		label->textToTexture(textFont, "Fodder"); // placing name above enemy
		label->draw(0.4, 0.4);
		strStream2 << "Health: " << fodderStats[0] << " Strength:  " << fodderStats[1] << " Speed:  " << fodderStats[2] << " Drops:  " << fodderStats[3];
		label->textToTexture(textFont, strStream2.str().c_str());
		label->draw(0.1,-0.9);

		if (playerStats[2]>fodderStats[2]){
			fodderStats[0]=fodderStats[0]-playerStats[1];
			if (fodderStats[0]<=0){
				label->textToTexture(textFont, "Fodder is Defeated"); 
				label->draw(-0.2, -0.2);
			}else{
				playerStats[0]=playerStats[0]-fodderStats[1];
				healthlost = fodderStats[1];
				if (playerStats[0]<=0){
					context.setState(context.getGameOverScreen());
				}
			}
		}

	}

	//brute fight
	if (type== 2){
		label->textToTexture(textFont, "Brute"); // placing name above enemy
		label->draw(0.3, 0.4);
		strStream2 << "Health: " << bruteStats[0] << " Strength:  " << bruteStats[1] << " Speed:  " << bruteStats[2] << " Drops:  " << bruteStats[3];
		label->textToTexture(textFont, strStream2.str().c_str());
		label->draw(0.4,-0.9);

		if (playerStats[2]>bruteStats[2]||bruteStats[0]<1 ){
			bruteStats[0]=bruteStats[0]-playerStats[1];
			if (bruteStats[0]<=0){
				label->textToTexture(textFont, "Brute is Defeated"); 
				label->draw(-0.2, -0.2);
			}else{
			 playerStats[0]=playerStats[0]-bruteStats[1];
			 healthlost = bruteStats[1];
			 if (playerStats[0]<=0){
			 context.setState(context.getGameOverScreen());

			 }
			 }
		}
	}


	//raider fight
	if (type== 3){
		label->textToTexture(textFont, "Raider"); // placing name above enemy
		label->draw(0.4, 0.4);
		strStream2  << "Health: " <<raiderStats[0] << " Strength:  " << raiderStats[1] << " Speed:  " << raiderStats[2] << " Drops:  " << raiderStats[3];
		label->textToTexture(textFont, strStream2.str().c_str());
		label->draw(0.1,-0.9);

		if (playerStats[2]>raiderStats[2] ||raiderStats[0]>0){
			raiderStats[0]=raiderStats[0]-playerStats[1];
			if (raiderStats[0]<=0){
				label->textToTexture(textFont, "Raider is Defeated"); 
				label->draw(-0.2, -0.2);
			}else{
				playerStats[0]=playerStats[0]-raiderStats[1];
				healthlost = raiderStats[1];
				if (playerStats[0]<=0){
					context.setState(context.getGameOverScreen());
				}
			}
		}else{
			playerStats[0]=playerStats[0]-raiderStats[1];
			healthlost = raiderStats[1];
			if (playerStats[0]<=0){
				context.setState(context.getGameOverScreen());
			}else{
				raiderStats[0]=raiderStats[0]-playerStats[1];
				if (raiderStats[0]<=0){
					label->textToTexture(textFont, "Raider is Defeated"); 
					label->draw(-0.2, -0.2);

				}
			}
		}
	}

	if((fodder+brute+raider)==0){
		context.setState(context.getGameOverScreen());
	}


	SDL_GL_SwapWindow(window);

}