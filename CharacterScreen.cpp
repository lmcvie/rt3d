#include "CharacterScreen.h"
#include "game.h"

void CharacterScreen::init(Game& context) {
	label = new Label();
}

bool CharacterScreen::handleSDLEvent(SDL_Event const &sdlEvent, Game &context) {
	bool running = true;
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		if (sdlEvent.key.keysym.sym == SDLK_RETURN)
		{
			context.setState(context.getPlayState());
			running = true;
		}
		if (sdlEvent.key.keysym.sym == SDLK_ESCAPE)
		{
			running = false;
			SDL_DestroyWindow(context.window);
			SDL_Quit();
		}
		return running;
	}
}

void CharacterScreen::draw(SDL_Window* window, Game& context) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	label->textToTexture(textFont,"Dummy Character Screen");
	label->draw(-0.1,0.5);
	SDL_GL_SwapWindow(window);

}