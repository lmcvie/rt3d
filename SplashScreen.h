#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

#include "GameState.h"
#include <SDL.h>
#include "label.h"

class Game;

class SplashScreen: public GameState {
public:
	~SplashScreen() { return; } // need a virtual destructor
	void draw(SDL_Window * window, Game& context);
	//virtual void init(Game * context) = 0;
	void init(Game &context);
	// Not using update function yet
	// virtual void update(void) = 0
	bool handleSDLEvent(SDL_Event const &sdlEvent, Game &context);
private:
	Label* label;
	clock_t lastTime, currentTime;
	TTF_Font* textFont;	
};

#endif
