#ifndef COMBATSCREEN_H
#define COMBATSCREEN_H

#include "GameState.h"
#include <SDL.h>
#include "label.h"
#include <fstream>
#include <iostream>

using namespace std;

class Game;

class CombatScreen: public GameState {
public:
	~CombatScreen() { return; } // need a virtual destructor
	void draw(SDL_Window * window, Game& context);
	//virtual void init(Game * context) = 0;
	void init(Game &context);
	bool handleSDLEvent(SDL_Event const &sdlEvent, Game &context);
private:
	int fodderStats[4];
	int bruteStats[4];
	int raiderStats[4];
	int i;
	int healthlost;
	Label* label;
	TTF_Font* textFont;	
};

#endif
