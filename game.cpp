#include "game.h"
#include "label.h"
#include <GL/glew.h>



Game::Game() {
	window = setupRC(glContext);
	playState = new PlayState();
	mainMenuState = new MainMenuState();
	splashScreen = new SplashScreen();
	characterScreen = new CharacterScreen();
	combatScreen = new CombatScreen();
	creditScreen = new CreditScreen();
	gameoverScreen = new GameOverScreen();


	currentState = splashScreen;

	splashScreen->init(*this);
	playState->init(*this);
	mainMenuState->init(*this);
	characterScreen->init(*this);
	combatScreen->init(*this);
	creditScreen->init(*this);
	gameoverScreen->init(*this);
}

void Game::run() {
	//SDL_GLContext glContext; // OpenGL context handle

	// Create window and render context
	//init(); // initialise the OpenGL and game variables 

	bool running = true; // set running to true
	SDL_Event sdlEvent; // variable to detect SDL events

	std::cout << "Progress: About to enter main loop" << std::endl;

	while (running)	
	{
		while (SDL_PollEvent(&sdlEvent))
		{
			if (sdlEvent.type == SDL_QUIT)
				running = false;
			else
				running = currentState->handleSDLEvent(sdlEvent, *this);
		}
		currentState->draw(window, *this);
	}
}

SDL_Window * Game::setupRC(SDL_GLContext &context)
{
	TTF_Font* textFont;
	SDL_Window *window;
	if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
		Label::exitFatalError("Unable to initialize SDL"); 

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); 
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); 

	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

	window = SDL_CreateWindow("SDL OpenGL Demo for GED",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) 
		Label::exitFatalError("Unable to create window");

	context = SDL_GL_CreateContext(window); 
	SDL_GL_SetSwapInterval(1); 

	if (TTF_Init()== -1)
		Label::exitFatalError("TTF failed to initialise.");

	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	if (textFont == NULL)
		Label::exitFatalError("Failed to open font.");

	return window;
}
