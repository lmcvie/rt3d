#include "PlayState.h"
#include "game.h"
#include "CombatScreen.h"


int brute =3, fodder=5, raider=1;
int type=0;
int playerStats[4]={10,10,10,0};


void PlayState::init(Game &context) {
	xpos = 0.0f;
	ypos = 0.0f;
	xsize = 0.1f;
	ysize = 0.1f;

	healthpack=2;
	combatpack=2;
	stimulant=1;

	fullhealth = 10;


	glClearColor(0.0, 0.0, 0.0, 0.0);

	raiderXpos = (float)rand()/RAND_MAX - 0.75f;
	raiderYpos = (float)rand()/RAND_MAX - 0.75f;

	stimulantXpos = (float)rand()/RAND_MAX - 0.95f;
	stimulantYpos = (float)rand()/RAND_MAX - 0.95f;

	label = new Label();


	for (int i =0; i<5; i++){
		FodderArrayXpos[i]=(float)rand()/RAND_MAX - 0.55f;
		FodderArrayYpos[i]=(float)rand()/RAND_MAX - 0.55f;
	}

	for (int i=0; i<3; i++){
		BruteArrayXpos[i]=(float)rand()/RAND_MAX - 0.65f;
		BruteArrayYpos[i]=(float)rand()/RAND_MAX - 0.65f;
	}

	for (int i =0; i<2; i++){
		healthpackXpos[i]=(float)rand()/RAND_MAX - 0.85f;
		healthpackYpos[i]=(float)rand()/RAND_MAX - 0.85f;
		combatpackXpos[i]=(float)rand()/RAND_MAX - 0.45f;
		combatpackYpos[i]=(float)rand()/RAND_MAX - 0.45f;
	}
}
void PlayState::draw(SDL_Window * window, Game& context) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	void glutMainLoop(void);

	//draw player
	glColor3f(1.0,1.0,1.0);
	glBegin(GL_POLYGON);
	glVertex3f (xpos, ypos, 0.0); // first corner
	glVertex3f (xpos+xsize, ypos, 0.0); // second corner
	glVertex3f (xpos+xsize, ypos+ysize, 0.0); // third corner
	glVertex3f (xpos, ypos+ysize, 0.0); // fourth corner
	glEnd();

	label->textToTexture(textFont, "Player"); // placing name above player
	label->draw(xpos+(xsize/2.0f), ypos+ysize);


	// draw Fodder
	if (fodder > 0){
		for (int i=0; i<fodder; i++){
			glClear(GL_POLYGON);
			glColor3f(0.0,1.0,0.0);
			glBegin(GL_POLYGON);
			glVertex3f (FodderArrayXpos[i],FodderArrayYpos[i], 0.0); // first corner
			glVertex3f (FodderArrayXpos[i]+xsize,FodderArrayYpos[i], 0.0); // second corner
			glVertex3f (FodderArrayXpos[i]+xsize,FodderArrayYpos[i]+ysize, 0.0); // third corner
			glVertex3f (FodderArrayXpos[i], FodderArrayYpos[i]+ysize, 0.0); // fourth corner
			glEnd();


			label->textToTexture(textFont, "Fodder"); // placing name above enemy
			label->draw(FodderArrayXpos[i]+xsize, FodderArrayYpos[i]+ysize);
			glColor3f(1.0,1.0,1.0);

			//Collision check for fodder

			if ( (FodderArrayXpos[i] >= xpos) && (FodderArrayXpos[i]+(xsize/2) <= xpos+xsize)	
				&& (FodderArrayYpos[i] >= ypos) && (FodderArrayYpos[i]+(ysize/2) <= ypos+ysize) ) 
			{

				fodder--;
				type=1;
				for (int i =0; i<fodder; i++){
					FodderArrayXpos[i]=(float)rand()/RAND_MAX - 0.55f;
					FodderArrayYpos[i]=(float)rand()/RAND_MAX - 0.55f;

				}

				context.setState(context.getCombatScreen());

			}
		}
	}
	//draw Brute
	if (brute > 0){
		for (int i=0; i<brute; i++){
			glClear(GL_POLYGON);
			glColor3f(1.0,1.0,0.0);
			glBegin(GL_POLYGON);
			glVertex3f (BruteArrayXpos[i],BruteArrayYpos[i], 0.0); // first corner
			glVertex3f (BruteArrayXpos[i]+xsize,BruteArrayYpos[i], 0.0); // second corner
			glVertex3f (BruteArrayXpos[i]+xsize,BruteArrayYpos[i]+ysize, 0.0); // third corner
			glVertex3f (BruteArrayXpos[i], BruteArrayYpos[i]+ysize, 0.0); // fourth corner
			glEnd();

			label->textToTexture(textFont, "Brute"); // placing name above player
			label->draw(BruteArrayXpos[i]+xsize, BruteArrayYpos[i]+ysize);
			glColor3f(1.0,1.0,1.0);

			// Collision check for brute
			if ( (BruteArrayXpos[i]>= xpos) && (BruteArrayXpos[i]+(xsize/2) <= xpos+xsize)	// cursor surrounds target in x
				&& (BruteArrayYpos[i] >= ypos) && (BruteArrayYpos[i]+(ysize/2) <= ypos+ysize) ) // cursor surrounds target in y
			{
				brute--;
				type =2;
				for (int i=0; i<brute; i++){
					BruteArrayXpos[i]=(float)rand()/RAND_MAX - 0.65f;
					BruteArrayYpos[i]=(float)rand()/RAND_MAX - 0.65f;
				}
				context.setState(context.getCombatScreen());

			}	
		}
	}

	//draw Raider
	if (raider > 0){
		glClear(GL_POLYGON);
		glColor3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);
		glVertex3f (raiderXpos,raiderYpos, 0.0); // first corner
		glVertex3f (raiderXpos+xsize,raiderYpos, 0.0); // second corner
		glVertex3f (raiderXpos+xsize,raiderYpos+ysize, 0.0); // third corner
		glVertex3f (raiderXpos,raiderYpos+ysize, 0.0); // fourth corner
		glEnd();

		label->textToTexture(textFont, "Raider"); // placing name above player
		label->draw(raiderXpos+xsize,raiderYpos+ysize);
		glColor3f(1.0,1.0,1.0);

		// Collision check for raider
		if ( (raiderXpos>= xpos) && (raiderXpos+(xsize/2) <= xpos+xsize)	// cursor surrounds target in x
			&& (raiderYpos >= ypos) && (raiderYpos+(ysize/2) <= ypos+ysize) ) // cursor surrounds target in y
		{
			raider--;
			type =3;
			raiderXpos = (float)rand()/RAND_MAX - 0.75f;
			raiderYpos = (float)rand()/RAND_MAX - 0.75f;
			context.setState(context.getCombatScreen());


		}	
	}

	// Print out the player stats
	std::stringstream strStream;
	strStream << "  Health:   " << playerStats[0] << "  Strength:   " << playerStats[1] << "  Speed:   " << playerStats[2] << "  Winnings:   " <<playerStats[3];


	label->textToTexture(textFont, strStream.str().c_str());
	label->draw(-0.9,-0.9);

	if((fodder+brute+raider)<=0){
		context.setState(context.getGameOverScreen());
	}





	//drawing healthpacks
	if (healthpack > 0){
		for (int i=0; i<healthpack; i++){
			glClear(GL_POLYGON);
			glColor3f(1.0,0.0,0.0);
			glBegin(GL_POLYGON);
			glVertex3f (healthpackXpos[i],healthpackYpos[i], 0.0); // first corner
			glVertex3f (healthpackXpos[i]+(xsize/2),healthpackYpos[i], 0.0); // second corner
			glVertex3f (healthpackXpos[i]+(xsize/2),healthpackYpos[i]+(ysize/2), 0.0); // third corner
			glVertex3f (healthpackXpos[i], healthpackYpos[i]+(ysize/2), 0.0); // fourth corner
			glEnd();


			label->textToTexture(textFont, "Health Pack"); // placing name above item
			label->draw(healthpackXpos[i]+(xsize/2.0f), healthpackYpos[i]+ysize);
			glColor3f(1.0,1.0,1.0);

			//Collision check for health pack

			if ( (healthpackXpos[i] >= xpos) && (healthpackXpos[i]+(xsize/2) <= xpos+xsize)	
				&& (healthpackYpos[i] >= ypos) && (healthpackYpos[i]+(ysize/2) <= ypos+ysize) ) 
			{

				for (int i=0; i<healthpack; i++){
					healthpackXpos[i] = (float)rand()/RAND_MAX - 0.85f;
					healthpackYpos[i] = (float)rand()/RAND_MAX - 0.85f;
				}
				healthpack--;
				
				if(playerStats[0]>=10){

					playerStats[0]++;
					fullhealth = playerStats[0];
				}else{
					playerStats[0]= fullhealth;
				}
			}
		}
	}

	//drawing combatpacks
	if (combatpack > 0){
		for (int i=0; i<combatpack; i++){
			glClear(GL_POLYGON);
			glColor3f(0.0,0.0,1.0);
			glBegin(GL_POLYGON);
			glVertex3f (combatpackXpos[i],combatpackYpos[i], 0.0); // first corner
			glVertex3f (combatpackXpos[i]+(xsize/2),combatpackYpos[i], 0.0); // second corner
			glVertex3f (combatpackXpos[i]+(xsize/2),combatpackYpos[i]+(ysize/2), 0.0); // third corner
			glVertex3f (combatpackXpos[i],combatpackYpos[i]+(ysize/2), 0.0); // fourth corner
			glEnd();


			label->textToTexture(textFont, "Combat Pack"); // placing name above item
			label->draw(combatpackXpos[i]+(xsize/2.0f),combatpackYpos[i]+ysize);
			glColor3f(1.0,1.0,1.0);

			//Collision check for combat pack

			if ( (combatpackXpos[i] >= xpos) && (combatpackXpos[i]+(xsize/2) <= xpos+xsize)	
				&& (combatpackYpos[i] >= ypos) && (combatpackYpos[i]+(ysize/2) <= ypos+ysize) ) 
			{

				for (int i=0; i<combatpack; i++){
					combatpackXpos[i] = (float)rand()/RAND_MAX - 0.45f;
					combatpackYpos[i] = (float)rand()/RAND_MAX - 0.45f;
				}
				combatpack--;
				playerStats[1]++;
				
			}
		}
	}

	//drawing stimulant
	if (stimulant > 0){
		for (int i=0; i<stimulant; i++){
			glClear(GL_POLYGON);
			glColor3f(0.0,1.0,0.0);
			glBegin(GL_POLYGON);
			glVertex3f (stimulantXpos,stimulantYpos, 0.0); // first corner
			glVertex3f (stimulantXpos+(xsize/2),stimulantYpos, 0.0); // second corner
			glVertex3f (stimulantXpos+(xsize/2),stimulantYpos+(ysize/2), 0.0); // third corner
			glVertex3f (stimulantXpos,stimulantYpos+(ysize/2), 0.0); // fourth corner
			glEnd();


			label->textToTexture(textFont, "Stimulant"); // placing name above item
			label->draw(stimulantXpos+(xsize/2.0f),stimulantYpos+ysize);
			glColor3f(1.0,1.0,1.0);

			//Collision check for stimulant

			if ( (stimulantXpos>= xpos) && (stimulantXpos+(xsize/2) <= xpos+xsize)	
				&& (stimulantYpos>= ypos) && (stimulantYpos+(ysize/2) <= ypos+ysize) ) 
			{

					stimulantXpos = (float)rand()/RAND_MAX - 0.95f;
					stimulantYpos = (float)rand()/RAND_MAX - 0.95f;
				stimulant--;
				playerStats[2]++;
				
			}
		}
	}

	SDL_GL_SwapWindow(window); // swap buffers
}

bool PlayState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context) {
	if (sdlEvent.type == SDL_KEYDOWN) {
		switch( sdlEvent.key.keysym.sym ) {
		case SDLK_UP:
		case 'w': case 'W':
			if(( ypos + 0.05) < (0.85))
				ypos += 0.05f;
			break;
		case SDLK_DOWN:
		case 's': case 'S':
			if(( ypos - 0.05) > (-1.0))
				ypos -= 0.05f;
			break;
		case SDLK_LEFT:
		case 'a': case 'A':
			if (( xpos - 0.05 ) > (-1.0))
				xpos -= 0.05f;
			break;
		case SDLK_RIGHT:
		case 'd': case 'D':
			if (( xpos + 0.05) < (0.85))
				xpos += 0.05f;
			break;
		case SDLK_ESCAPE:
			context.setState(context.getMainMenuState());
		default:
			break;
		}
		return true;
	}
}
