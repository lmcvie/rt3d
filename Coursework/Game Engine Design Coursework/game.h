#ifndef GAME_H
#define GAME_H

#include <SDL.h>
#include <SDL_ttf.h>
#include <GL/glew.h>

// C stdlib and C time libraries for rand and time functions
#include <cstdlib>
#include <ctime>

// stringstream and string
#include <sstream>
#include <string>
#include "label.h"
#include "GameState.h"
#include "PlayState.h"
#include "MainMenuState.h"


class Game
{
	public:
		Game();
		void run();
		SDL_Window * setupRC(SDL_GLContext &context);
	private:
		SDL_Window *window;
		SDL_GLContext glContext;

		void setState(GameState * newState){currentState = newState;}

		GameState *getCurrentState(){return currentState;}
		GameState *getPlayState(){return playState;}
		GameState *getMainMenuState(){return mainMenuState;}

		friend class GameState;
		friend class PlayState;
		friend class MainMenuState;

		GameState * currentState;
		GameState * playState;
		GameState * mainMenuState;
};
#endif