#ifndef PLAYSTATE_H
#define PLAYSTATE_H

#include <SDL.h>
#include "GameState.h"
#include "label.h"

class Game;

class PlayState: public GameState {
public:
	~PlayState() { return; } // need a virtual destructor
	void draw(SDL_Window * window, Game& context);
	//virtual void init(Game * context) = 0;
	void init(Game &context);
	// Not using update function yet
	// virtual void update(void) = 0
	bool handleSDLEvent(SDL_Event const &sdlEvent, Game &context);

private:
	float xpos, ypos, xsize, ysize;
	float targetXPos, targetYPos, targetXSize, targetYSize;

	int score;

	Label* label;
	Game* game;

	clock_t lastTime, currentTime;
	TTF_Font* textFont;	
};

#endif
