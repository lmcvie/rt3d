#include <GL/glew.h>
#include <SDL.h>
#include <iostream>
#include <fstream>
#include <string>
#include "Game.h"


using namespace std;

#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif




// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {


	Game *newGame = new Game();

	//newGame->init();
	newGame->run();

	delete newGame;
		
    return 0;
}