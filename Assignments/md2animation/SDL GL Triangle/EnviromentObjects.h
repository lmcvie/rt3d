#ifndef ENVIROMENT_OBJECTS_H
#define ENVIROMENT_OBJECTS_H

#include <SDL.h>
#include "md2model.h"
#include <GL/glew.h>
#include "rt3d.h"
#include "rt3dObjLoader.h"
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Rect.h"
using namespace std;


class EnviromentObjects{
public:
	EnviromentObjects();
	~EnviromentObjects() { return; } // need a virtual destructor
	
	GLuint GetObjTexture(int i){ return Objtextures[i];}
	void init();
	//cubes that will be textured
	void drawCube();;
	//Models
	void drawAmmo();
	void drawCar();
	rect getRect();
	bool collidesWith(rect cRect);

private:
	GLuint Objtextures[11];
	GLuint meshIndexCount;
	GLuint meshObjects[7];

	GLuint md2VertCount[7];
	md2model tmpModel, tmpModel2;
	rect carRect;
	rect fountainRect;
	rect building1Rect1;
	rect building1Rect2;
	rect building2Rect;
	rect ammoRect;
	rect turretRect;
	rect fence1Rect;
	rect fence2Rect;
	rect fence3Rect;
	rect fence4Rect;
};

#endif
