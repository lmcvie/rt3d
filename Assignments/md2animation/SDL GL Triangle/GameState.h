#ifndef GAMESTATE_H
#define GAMESTATE_H

#include <SDL.h>


class Game;
// Game.h will still need to be included in the state implementation files

// Abstract game state class
// different game states will inherit from this
class GameState {
public:
	virtual ~GameState() { return; } // need a virtual destructor
	virtual void draw(SDL_Window * window, Game& context) = 0;
	virtual void init(Game &context) = 0;
	virtual bool update(Game &context) = 0;
};

#endif
