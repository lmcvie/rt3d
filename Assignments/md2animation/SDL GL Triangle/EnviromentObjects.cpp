#include "EnviromentObjects.h"
EnviromentObjects::EnviromentObjects(){
	carRect.setRect(42.0f,13.0f,5.0f,8.0f);
	building1Rect1.setRect(23.0f,-10.0f,25.0f,13.0f);
	building1Rect2.setRect(23.0f,27.0f,25.0f,13.0f);
	building2Rect.setRect(-27.0f,-10.0f,16.0f,16.0f);
	ammoRect.setRect(10.0f,38.0f,3.0f,3.0f);
	fence1Rect.setRect(-27.0f,5.0f,0.5f,35.0f);
	fence2Rect.setRect(-27.0f,40.0f,50.0f,0.5f);
	fence3Rect.setRect(-7.0f,-10.0f,50.0f,0.5f);
	fence4Rect.setRect(48.0f,3.0f,0.5f,37.0f);
}

bool EnviromentObjects::collidesWith(rect cRect){
	if (cRect.intersects(carRect)){
		return true;
	}

	if (cRect.intersects(building1Rect1)){
		return true;
	}

	if (cRect.intersects(building1Rect2)){
		return true;
	}

	if (cRect.intersects(building2Rect)){
		return true;
	}

	if (cRect.intersects(fence1Rect)){
		return true;
	}

	if (cRect.intersects(fence2Rect)){
		return true;
	}
	if (cRect.intersects(fence3Rect)){
		return true;
	}
	if (cRect.intersects(fence4Rect)){
		return true;
	}

	if (cRect.intersects(ammoRect)){
		return true;
	}
}

	
void EnviromentObjects::init(){
	meshIndexCount = 0;
	md2VertCount[0] = 0;
	md2VertCount[1] =0;
	md2VertCount[2] =0;
	md2VertCount[3] = 0;
	md2VertCount[4] =0;
	md2VertCount[5] =0;
	md2VertCount[6] = 0;
	
	//load object
	vector<GLfloat> verts;
	vector<GLfloat> norms;
	vector<GLfloat> tex_coords;
	vector<GLuint> indices;
	rt3d::loadObj("cube.obj", verts, norms, tex_coords, indices);
	GLuint size = indices.size();
	meshIndexCount = size;

	

	//create ground mesh
	meshObjects[0] = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());


   
	
//load road
	Objtextures[0] = rt3d::loadBitmap("road3.bmp");
	 //pavement left
	Objtextures[1] = rt3d::loadBitmap("concrete.bmp");
	
	
	//building type 1
	Objtextures[2] = rt3d::loadBitmap("Building1.bmp");

	//building type 2
	Objtextures[3] = rt3d::loadBitmap("Building2.bmp");
	

	//car
	Objtextures[4] = rt3d::loadBitmap("car.bmp");
	meshObjects[1] = tmpModel.ReadMD2Model("car.MD2");
	md2VertCount[0]= tmpModel.getVertDataSize();


	//ammo
	Objtextures[9] = rt3d::loadBitmap("ammo.bmp");
	meshObjects[6] = tmpModel2.ReadMD2Model("ammo.MD2");
	md2VertCount[5]=tmpModel2.getVertDataSize();

	//fence
	Objtextures[10]= rt3d::loadBitmap("Concretefence.bmp");

}
	

void EnviromentObjects::drawCube(){
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	}


void EnviromentObjects::drawAmmo(){
	rt3d::updateMesh(meshObjects[6],RT3D_VERTEX,tmpModel2.getAnimVerts(),tmpModel2.getVertDataSize());
	rt3d::drawMesh(meshObjects[6], md2VertCount[5]/3, GL_TRIANGLES);
}



void EnviromentObjects::drawCar(){
	//update the mesh with new vertex data
	rt3d::updateMesh(meshObjects[1],RT3D_VERTEX,tmpModel.getAnimVerts(),tmpModel.getVertDataSize());
	rt3d::drawMesh(meshObjects[1], md2VertCount[0]/3, GL_TRIANGLES);
}


