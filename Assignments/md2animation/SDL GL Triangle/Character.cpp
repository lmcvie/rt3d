#include "Character.h"
#define DEG_TO_RADIAN 0.017453293
Character::Character(){
//player vector


x=0.0f;
y=0.4f;
z=2.0f;
xsize=1.5;
zsize=1.5;
exsize=1.3;
ezsize=1.3;
enemy.x=10.0f;
enemy.y=1.0f;
enemy.z=0.0f;
enemyRect.setRect(enemy.x,enemy.z,exsize,ezsize);

}
	
void Character::SetPlayerPos(glm::vec3 PlayerPos){
	x=PlayerPos.x;
	y=PlayerPos.y;
	z=PlayerPos.z;
	glm::vec3 test(x,y,z);
	Player=test;
	playerRect.setRect(Player.x,Player.z,xsize,zsize);
}



bool Character::collidesWith(){
	return playerRect.intersects(enemyRect);
}

void Character::SetEnemyPos(glm::vec3 EnemyPos){
	enemy.x=EnemyPos.x;
	enemy.y=EnemyPos.y;
	enemy.z=EnemyPos.z;
	enemyRect.setRect(enemy.x,enemy.z,3.5,3.5);
}
	
void Character::init(){
	//player
	currentAnim=0;
	md2VertCount = 0;
	md2VertCount2 = 0;
	Playertexture = rt3d::loadBitmap("hayden.bmp");
	PlayerObject = tmpModel.ReadMD2Model("hayden-tris.MD2");

	Enemytexture = rt3d::loadBitmap("FloatingEye.bmp");
	EnemyObject = tmpModel2.ReadMD2Model("caco.MD2");

	md2VertCount = tmpModel.getVertDataSize();
	md2VertCount2 = tmpModel2.getVertDataSize();
	

}

void Character::SetCurrentAnim(int Anim){
	currentAnim=Anim;
	
}


void Character::drawPlayer(){

	tmpModel.Animate(currentAnim,0.1);
	rt3d::updateMesh(PlayerObject,RT3D_VERTEX,tmpModel.getAnimVerts(),tmpModel.getVertDataSize());
	rt3d::drawMesh(PlayerObject, md2VertCount/3, GL_TRIANGLES);
	}

void Character::drawEnemy(){
	rt3d::updateMesh(EnemyObject,RT3D_VERTEX,tmpModel2.getAnimVerts(),tmpModel2.getVertDataSize());
	rt3d::drawMesh(EnemyObject, md2VertCount2/3, GL_TRIANGLES);
}

rect Character::getRect(int Cno){
	if (Cno==0){
	return playerRect;
	}
	if (Cno==1){
		return enemyRect;
	}
}