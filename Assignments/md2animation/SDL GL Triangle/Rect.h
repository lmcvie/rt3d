#ifndef RECT_H
#define RECT_H

class rect{
public:
	rect();
	void setRect(float x, float y, float w, float h);
	bool intersects(rect &r);
private:
	float left, top, right, bottom;
};
#endif