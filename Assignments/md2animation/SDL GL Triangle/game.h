#ifndef GAME_H
#define GAME_H

#include <SDL.h>
#include <SDL_ttf.h>
#include <GL/glew.h>

// C stdlib and C time libraries for rand and time functions
#include <cstdlib>
#include <ctime>

// stringstream and string
#include <sstream>
#include <string>
//#include "label.h"
#include "GameState.h"
#include "PlayState.h"
#include "md2model.h"

#include "rt3d.h"
#include "rt3dObjLoader.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>
#include <GL/glew.h>




class Game
{
public:
	Game();
	void run();
	SDL_Window * setupRC(SDL_GLContext &context);
	
private:
	SDL_Window *window;
	SDL_GLContext glContext;

	void setState(GameState * newState){currentState = newState;}

	GameState *getCurrentState(){return currentState;}
	GameState *getMenuState(){return menuState;}
	GameState *getPlayState(){return playState;}

	friend class GameState;
	friend class MenuState;
	friend class PlayState;

	GameState * currentState;
	GameState * menuState;
	GameState * playState;

};
#endif