#include "PlayState.h"
#define DEG_TO_RADIAN 0.017453293
using namespace std;


stack<glm::mat4> mvStack; 



///////////////////////////////////////////
//light and material
rt3d::lightStruct light0 = {
	{0.3f, 0.3f, 0.3f, 1.0f}, // ambient
	{1.0f, 1.0f, 1.0f, 1.0f}, // diffuse
	{1.0f, 1.0f, 1.0f, 1.0f}, // specular
	{-10.0f, 10.0f, 10.0f, 1.0f}  // position
};
glm::vec4 lightPos(-20.0f, 20.0f, 20.0f, 1.0f); //light position

rt3d::materialStruct material0 = {
	{0.2f, 0.4f, 0.2f, 1.0f}, // ambient
	{0.5f, 1.0f, 0.5f, 1.0f}, // diffuse
	{0.0f, 0.1f, 0.0f, 1.0f}, // specular
	2.0f  // shininess
};
rt3d::materialStruct material1 = {
	{0.4f, 0.4f, 1.0f, 1.0f}, // ambient
	{0.8f, 0.8f, 1.0f, 1.0f}, // diffuse
	{0.8f, 0.8f, 0.8f, 1.0f}, // specular
	1.0f  // shininess
};








void PlayState::init(Game& context) {

anim=0;


// rotation
r = 1.0f;

//scale
s = 1.0f;

cam = new Camera();
player = new Character();
enemy1 = new Character();
Skyboxes = new Skybox();
Obj = new EnviromentObjects();
	//label = new Label();
    Skyboxes->init();
	// Set shaders,material and light source
	ShaderProgram = rt3d::initShaders("phong-tex.vert","phong-tex.frag");
	rt3d::setLight(ShaderProgram, light0);
	rt3d::setMaterial(ShaderProgram, material0);
	player->init();
	enemy1->init();
	Obj-> init();
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);	
	
}

void PlayState::MoveTowards(){
	speedfactor=0.03;
	GLfloat tempx= enemy1->GetEnemyPos().x;
	GLfloat tempz= enemy1->GetEnemyPos().z;
	enemy1->SetEnemyPos(glm::vec3 (tempx += (cos(enemyr)*speedfactor),enemy1->GetEnemyPos().y,tempz += (sin(enemyr)*speedfactor)));
	//enemy1->SetEnemyPos(rt3d::moveForward(enemy1->GetEnemyPos(),enemyr+90,0.1f));
}





bool PlayState::update(Game &context) {
	bool running = true;
	forward=false;
	right=false;
	speedfactor+=0.05;
	anim=0;
	player->SetCurrentAnim(anim);
	
	if (player->collidesWith()){
		forward=true;
		right=true;
		
	}

	if(Obj->collidesWith(player->getRect(0))){
		forward=true;
		right=true;

		if(Obj->collidesWith(enemy1->getRect(1))){
		forward=true;
		}
	}


	Uint8 *keys = SDL_GetKeyboardState(NULL);
	if ( keys[SDL_SCANCODE_W] ){
	    anim=1;
		if (forward==true){
		player->SetPlayerPos(glm::vec3 (player->GetPlayerPos().x-0.15,player->GetPlayerPos().y,player->GetPlayerPos().z+ 0.15));
		}
		player->SetCurrentAnim(anim);
		player->SetPlayerPos(rt3d::moveForward(player->GetPlayerPos(),r,0.1f));
	}

	if ( keys[SDL_SCANCODE_S] ){
		anim=1;
		if (forward==true){
		player->SetPlayerPos(glm::vec3 (player->GetPlayerPos().x+0.15,player->GetPlayerPos().y,player->GetPlayerPos().z -0.15));
		}
		player->SetCurrentAnim(anim);
		player->SetPlayerPos(rt3d::moveForward(player->GetPlayerPos(),r,-0.1f));
	}

	if ( keys[SDL_SCANCODE_A] ){
		anim=1;
		if (right==true){
			player->SetPlayerPos(glm::vec3 (player->GetPlayerPos().x+0.15,player->GetPlayerPos().y,player->GetPlayerPos().z+0.15));
		}
		player->SetCurrentAnim(anim);
		player->SetPlayerPos(rt3d::moveRight(player->GetPlayerPos(),r,-0.1f));
		
	}

	if ( keys[SDL_SCANCODE_D] ){
		anim=1;
		if (right==true){
			player->SetPlayerPos(glm::vec3 (player->GetPlayerPos().x-0.15,player->GetPlayerPos().y,player->GetPlayerPos().z -0.15));
		}
		player->SetCurrentAnim(anim);
		player->SetPlayerPos(rt3d::moveRight(player->GetPlayerPos(),r,0.1f));
	}

	MoveTowards();

	if ( keys[SDL_SCANCODE_R]){
		cam->SetEyeY((tempcam.y+0.1f));
	}

	if ( keys[SDL_SCANCODE_F]){
		cam->SetEyeY((tempcam.y-0.1f));
	}
	if ( keys[SDL_SCANCODE_COMMA] ) {
		r -= 1.0f;
	}
	if ( keys[SDL_SCANCODE_PERIOD] ) {
	   r += 1.0f;
	}

	if ( keys[SDL_SCANCODE_ESCAPE] ) {
		running=false;
	}
	
	return running;
}





void PlayState::draw(SDL_Window* window, Game& context) {
	// clear the screen
	glEnable(GL_CULL_FACE);
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
	
	glm::mat4 projection(1.0);
	projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,70.0f);

	

	GLfloat scale(1.0f); // just to allow easy scaling of complete scene

	
	// set base position for scene
	glm::mat4 modelview(1.0);	
	mvStack.push(modelview);
	tempcam = cam->GetEye();
	cam->SetEye(rt3d::moveForward(player->GetPlayerPos(),r,-4.0f));
	cam->SetEyeY(tempcam.y);
	
	cam->SetAt(rt3d::moveForward(player->GetPlayerPos(),r,2.0f));
	mvStack.top() = glm::lookAt(cam->GetEye(),cam->GetAt(),cam->GetUp());
	glm::vec4 tmp = mvStack.top()*lightPos;


	
	// draw a skybox
	glUseProgram(Skyboxes->GetSkyboxProgram());
	rt3d::setUniformMatrix4fv(Skyboxes->GetSkyboxProgram(), "projection", glm::value_ptr(projection));

	glDepthMask(GL_FALSE); // make sure depth test is off
	glm::mat3 mvRotOnlyMat3 = glm::mat3(mvStack.top());
	mvStack.push( glm::mat4(mvRotOnlyMat3) );
		
	// front
	mvStack.push( mvStack.top() );
	glBindTexture(GL_TEXTURE_2D, Skyboxes->getSkytexture(0));
	rt3d::setUniformMatrix4fv(Skyboxes->GetSkyboxProgram(), "modelview", glm::value_ptr(mvStack.top()));
	Skyboxes->drawSkyboxFront();
	

	// left
	glBindTexture(GL_TEXTURE_2D, Skyboxes->getSkytexture(1));
	mvStack.top() = glm::rotate(mvStack.top(),90.0f,glm::vec3(0.0f,1.0f,0.0f));
	rt3d::setUniformMatrix4fv(Skyboxes->GetSkyboxProgram(), "modelview", glm::value_ptr(mvStack.top()));
	Skyboxes->drawSkyboxLeft();


	// right

	glBindTexture(GL_TEXTURE_2D, Skyboxes->getSkytexture(2));
	mvStack.top() = glm::rotate(mvStack.top(),90.0f,glm::vec3(0.0f,1.0f,0.0f));
	rt3d::setUniformMatrix4fv(Skyboxes->GetSkyboxProgram(), "modelview", glm::value_ptr(mvStack.top()));
	Skyboxes->drawSkyboxRight();

		
	// back

	glBindTexture(GL_TEXTURE_2D, Skyboxes->getSkytexture(3));
	mvStack.top() = glm::rotate(mvStack.top(),90.0f,glm::vec3(0.0f,1.0f,0.0f));
	rt3d::setUniformMatrix4fv(Skyboxes->GetSkyboxProgram(), "modelview", glm::value_ptr(mvStack.top()));
	Skyboxes->drawSkyboxBack();
	

	//top
	glBindTexture(GL_TEXTURE_2D, Skyboxes->getSkytexture(4));
	mvStack.top() = glm::rotate(mvStack.top(),90.0f,glm::vec3(1.0f,0.0f,0.0f));
	mvStack.top() = glm::rotate(mvStack.top(),90.0f,glm::vec3(0.0f,0.0f,-1.0f));
	rt3d::setUniformMatrix4fv(Skyboxes->GetSkyboxProgram(), "modelview", glm::value_ptr(mvStack.top()));
	Skyboxes->drawSkyboxTop();
	mvStack.pop();
		

	
	light0.position[0] = tmp.x;
	light0.position[1] = tmp.y;
	light0.position[2] = tmp.z;
	
	
	glDepthMask(GL_TRUE); // make sure depth test is on

		
	mvStack.top() = glm::lookAt(cam->GetEye(),cam->GetAt(),cam->GetUp());

	glUseProgram(ShaderProgram);
	rt3d::setUniformMatrix4fv(ShaderProgram, "projection", glm::value_ptr(projection));
	rt3d::setLightPos(ShaderProgram, glm::value_ptr(tmp));
	rt3d::setLight(ShaderProgram, light0);
		
	// road
	glBindTexture(GL_TEXTURE_2D, Obj->GetObjTexture(0));
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-2.0f, 0.0f, -10.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(25.0f, 0.1f, 50.0f));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	rt3d::setMaterial(ShaderProgram, material1);
	Obj->drawCube();
	mvStack.pop();


//left pavement
	glBindTexture(GL_TEXTURE_2D, Obj->GetObjTexture(1));
	rt3d::setMaterial(ShaderProgram, material1);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-27.0f, 0.0f, -10.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(25.0f, 0.1f, 50.0f));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	Obj->drawCube();
	mvStack.pop();

	//right pavement
	glBindTexture(GL_TEXTURE_2D, Obj->GetObjTexture(1));
	rt3d::setMaterial(ShaderProgram, material1);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(23.0f, 0.0f, -10.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(25.0f, 0.1f, 50.0f));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	Obj->drawCube();
	mvStack.pop();
	
	//fence 1(4 of these)
	glBindTexture(GL_TEXTURE_2D, Obj->GetObjTexture(10));
	rt3d::setMaterial(ShaderProgram, material1);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-27.0f, 0.0f, 5.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.1f, 3.0f, 35.0f));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	Obj->drawCube();
	mvStack.pop();
	
	//fence 2
	glBindTexture(GL_TEXTURE_2D, Obj->GetObjTexture(10));
	rt3d::setMaterial(ShaderProgram, material1);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-27.0f, 0.0f, 40.0f));
	mvStack.top() = glm::rotate(mvStack.top(),90.0f,glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.1f, 3.0f, 50.0f));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	Obj->drawCube();
	mvStack.pop();

	//fence 3
	glBindTexture(GL_TEXTURE_2D, Obj->GetObjTexture(10));
	rt3d::setMaterial(ShaderProgram, material1);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-13.0f, 0.0f, -10.0f));
	mvStack.top() = glm::rotate(mvStack.top(),90.0f,glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.1f, 3.0f, 37.0f));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	Obj->drawCube();
	mvStack.pop();

	//fence 4
	glBindTexture(GL_TEXTURE_2D, Obj->GetObjTexture(10));
	rt3d::setMaterial(ShaderProgram, material1);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(48.0f, 0.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(0.1f, 3.0f, 37.0f));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	Obj->drawCube();
	mvStack.pop();



	//building type 1(4 of them)
	glBindTexture(GL_TEXTURE_2D, Obj->GetObjTexture(2));
	rt3d::setMaterial(ShaderProgram, material1);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(23.0f, 12.6f, -10.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(12.5f, 12.5f, 12.5f));
	mvStack.top() = glm::rotate(mvStack.top(),270.0f,glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(),180.0f,glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	Obj->drawCube();
	mvStack.pop();


	glBindTexture(GL_TEXTURE_2D,  Obj->GetObjTexture(2));
	rt3d::setMaterial(ShaderProgram, material1);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(35.5f, 12.6f, -10.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(12.5f, 12.5f,12.5f));
	mvStack.top() = glm::rotate(mvStack.top(),270.0f,glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(),180.0f,glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	Obj->drawCube();
	mvStack.pop();

	
	glBindTexture(GL_TEXTURE_2D,  Obj->GetObjTexture(2));
	rt3d::setMaterial(ShaderProgram, material1);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(35.5f, 12.6f, 40.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(12.5f, 12.5f,12.5f));
	mvStack.top() = glm::rotate(mvStack.top(),90.0f,glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(),180.0f,glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	Obj->drawCube();
	mvStack.pop();



	glBindTexture(GL_TEXTURE_2D,  Obj->GetObjTexture(2));
	rt3d::setMaterial(ShaderProgram, material1);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(48.0f, 12.6f, 40.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(12.5f, 12.5f,12.5f));
	mvStack.top() = glm::rotate(mvStack.top(),90.0f,glm::vec3(0.0f, 1.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(),180.0f,glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	Obj->drawCube();
	mvStack.pop();

	//building type 2
	glBindTexture(GL_TEXTURE_2D,  Obj->GetObjTexture(3));
	rt3d::setMaterial(ShaderProgram, material1);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(-27.0f, 15.1f, 5.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(15.0f, 15.0f,15.0f));
	mvStack.top() = glm::rotate(mvStack.top(),180.0f,glm::vec3(1.0f, 0.0f, 0.0f));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	Obj->drawCube();
	mvStack.pop();


	
	// draw the player
	glCullFace(GL_FRONT);
	glBindTexture(GL_TEXTURE_2D, player->GetPlayerTexture());
	rt3d::materialStruct tmpMaterial = material1;
	rt3d::setMaterial(ShaderProgram, tmpMaterial);
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(player->GetPlayerPos().x, 1.3f+player->GetPlayerPos().y, player->GetPlayerPos().z));
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(-11.0f, 0.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(0.0f, 0.0f, 1.0f));
	mvStack.top() = glm::rotate(mvStack.top(),r,glm::vec3(0.0f,0.0f,-1.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(scale*0.05, scale*0.05, scale*0.05));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	player->drawPlayer();
	mvStack.pop();
	glCullFace(GL_BACK);
	
	glm::vec3 temp = player->GetPlayerPos();
	temp.x= (player->GetPlayerPos().x-enemy1->GetEnemyPos().x);
	temp.z = (player->GetPlayerPos().z-enemy1->GetEnemyPos().z);
	double angle = (atan2(temp.z,temp.x))/DEG_TO_RADIAN;

	enemyr= angle;
	// draw the enemy
	glCullFace(GL_FRONT);
	glBindTexture(GL_TEXTURE_2D, enemy1->GetEnemyTexture());
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(enemy1->GetEnemyPos().x, 1.0f+enemy1->GetEnemyPos().y, enemy1->GetEnemyPos().z));
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(-11.0f, 0.0f, 0.0f));
	mvStack.top() = glm::rotate(mvStack.top(),enemyr,glm::vec3(0.0f,0.0f,-1.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(scale*0.02, scale*0.02, scale*0.02));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	enemy1->drawEnemy();
	mvStack.pop();
	glCullFace(GL_BACK);
	
	
	
	// draw the car
	glCullFace(GL_FRONT);
	glBindTexture(GL_TEXTURE_2D,  Obj->GetObjTexture(4));
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(45.0f, 0.0f,16.0f));
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(-11.0f, 0.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(scale*0.03, scale*0.03, scale*0.03));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	Obj->drawCar();
	mvStack.pop();
	glCullFace(GL_BACK);	
	
	// draw the ammo
	glCullFace(GL_FRONT);
	glBindTexture(GL_TEXTURE_2D, Obj->GetObjTexture(9));
	mvStack.push(mvStack.top());
	mvStack.top() = glm::translate(mvStack.top(), glm::vec3(10.0f, 0.0f,38.0f));
	mvStack.top() = glm::rotate(mvStack.top(), 90.0f, glm::vec3(-11.0f, 0.0f, 0.0f));
	mvStack.top() = glm::scale(mvStack.top(),glm::vec3(scale*0.08, scale*0.08, scale*0.08));
	rt3d::setUniformMatrix4fv(ShaderProgram, "modelview", glm::value_ptr(mvStack.top()));
	Obj->drawAmmo();
	mvStack.pop();
	glCullFace(GL_BACK);
	
	
	glDepthMask(GL_TRUE);

	SDL_GL_SwapWindow(window); // swap buffers
}