#ifndef PLAYSTATE_H
#define PLAYSTATE_H

#include "GameState.h"
#include <SDL.h>
#include "Game.h"
#include <GL/glew.h>
#include "rt3d.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "md2model.h"
#include "Camera.h"
#include "Character.h"
#include "Skybox.h"
#include "EnviromentObjects.h"
#include "Rect.h"
using namespace std;


class Game;

class PlayState: public GameState {
public:
	~PlayState() { return; } // need a virtual destructor
	void draw(SDL_Window * window, Game& context);
	void init(Game &context);
	bool update(Game &context);
	void MoveTowards();
private:
		
int anim;
GLfloat r ;
GLfloat speedfactor;
GLfloat enemyr;
GLfloat s ;
Camera *cam;
Character *player;
Character *enemy1;
Skybox *Skyboxes;
EnviromentObjects *Obj;
glm::vec3 tempcam;
bool forward;
bool right;


GLuint ShaderProgram;


};

#endif
