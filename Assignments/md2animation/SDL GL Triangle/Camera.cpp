#include "Camera.h"




Camera::Camera(){
//eye vector
eye.x=0.0f;
eye.y=3.7f;
eye.z=4.0f;

//at vector
at.x=0.0f;
at.y=1.0f;
at.z=2.0f;

//up vector
up.x=0.0f;
up.y=2.0f;
up.z=0.0f;
}

void Camera::SetEye(glm::vec3 eye2){
	eye.x= eye2.x;
	eye.y= eye2.y;
	eye.z= eye2.z;
}

void Camera::SetEyeY(GLfloat EyeY){
	eye.y=EyeY;

}

void Camera::SetAt(glm::vec3 at2){
	at.x= at2.x;
	at.y= at2.y;
	at.z= at2.z;
}

void Camera::SetUp(glm::vec3 at2){
	at.x= at2.x;
	at.y= at2.y;
	at.z= at2.z;
}