#ifndef CAMERA_H
#define CAMERA_H


#include <SDL.h>
#include <glm\glm.hpp>
#include <GL/glew.h>
//#include "label.h"


class Camera {
public:
	Camera();
	~Camera() { return; } // need a virtual destructor
	void SetEye(glm::vec3 eye2);
	void SetEyeY(GLfloat eyeY);
	
	glm::vec3 GetEye(){ return eye;}

	void SetAt(glm::vec3 at2);
	
	glm::vec3 GetAt(){ return at;}

	void SetUp(glm::vec3 up2);
	
	glm::vec3 GetUp(){ return up;}
private:
	glm::vec3 eye;
	glm::vec3 at;
	glm::vec3 up;
};

#endif
