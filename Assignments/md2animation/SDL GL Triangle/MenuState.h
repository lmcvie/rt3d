#ifndef MENUSTATE_H
#define MENUSTATE_H

#include "GameState.h"
#include <SDL.h>
#include "label.h"

class Game;

class MenuState: public GameState {
public:
	~MenuState() { return; } // need a virtual destructor
	void draw(SDL_Window * window, Game& context);
	void init(Game &context);
	bool update(Game &context);
private:
	label* label;
	TTF_Font* textFont;
	label* label;
};

#endif
