#include "MenuState.h"
#include "Game.h"


void MenuState::init(Game& context) {
//	label = new Label();
}

bool MenuState::update(Game &context) {
	bool running =true;
	Uint8 *keys = SDL_GetKeyboardState(NULL);
	if ( keys[SDL_SCANCODE_RETURN] ){
		context.setState(context.getPlayState());
	}
	return running;
}

void MenuState::draw(SDL_Window* window, Game& context) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	glClearColor(1.0f,0.5f,0.5f,1.0f);
	label->textToTexture(textFont,"Press Return To begin Game"); //display menu screen message
	label->draw(-0.3,0.3);
}