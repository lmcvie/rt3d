#ifndef CHARACTER_H
#define CHARACTER_H

#include <SDL.h>
#include "md2model.h"
#include <GL/glew.h>
#include "rt3d.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Rect.h"


class Character {
public:
	Character();
	~Character() { return; } // need a virtual destructor
	void SetPlayerPos(glm::vec3 PlayerPos);
	void SetEnemyPos(glm::vec3 EnemyPos);
	void SetCurrentAnim(int Anim);
	GLuint GetPlayerTexture(){ return Playertexture;}
	GLuint GetEnemyTexture(){ return Enemytexture;}
	void init();
	void drawPlayer();
	void drawEnemy();
	rect getRect(int Cno);
	bool collidesWith();
	glm::vec3 GetPlayerPos(){ return Player;}
	glm::vec3 GetEnemyPos(){ return enemy;}
private:
	
	GLfloat x,y,z,xsize,zsize,exsize,ezsize;
	glm::vec3 Player;
	glm::vec3 enemy;
	GLfloat rEnemy ;
	GLuint PlayerObject; 
	GLuint EnemyObject;
	GLuint Playertexture; 
	GLuint Enemytexture;
	GLuint md2VertCount, md2VertCount2;
	md2model tmpModel, tmpModel2;
	int currentAnim;
	rect playerRect;
	rect enemyRect;
};

#endif
