#include "Skybox.h"

GLfloat SkyboxVerts[] = { 2.0f,-2.0f,-2.0f,
						2.0f,2.0f,-2.0f,
						-2.0f,2.0f,-2.0f,
						-2.0f,-2.0f,-2.0f};

GLuint SkyboxIndices[] = {	0,1,2, 2,3,0}; 

GLfloat SkyboxTexCoords[] = {0.0f, 1.0f,
							0.0f, 0.0f,
							1.0f, 0.0f,
							1.0f, 1.0f };

Skybox::Skybox(){


}
	


	
void Skybox::init(){
	SkyboxProgram = rt3d::initShaders("textured.vert","textured.frag");
	SkyboxIndexCount = 6;

	SkyboxVertCount = 4;
	
	//create Skybox mesh
	SkyboxObjects = rt3d::createMesh(SkyboxVertCount, SkyboxVerts,nullptr,SkyboxVerts,SkyboxTexCoords,SkyboxIndexCount,SkyboxIndices);
	

	//Skybox textures
	SkyboxTextures[0] = rt3d::loadBitmap ("skyfront.bmp");
	SkyboxTextures[1] = rt3d::loadBitmap ("skyleft.bmp");
	SkyboxTextures[2] = rt3d::loadBitmap ("skyright.bmp");
	SkyboxTextures[3] = rt3d::loadBitmap ("skyback.bmp");
	SkyboxTextures[4] = rt3d::loadBitmap ("skytop.bmp");

}

void Skybox::drawSkyboxFront(){
	rt3d::drawIndexedMesh(SkyboxObjects,SkyboxIndexCount,GL_TRIANGLES);
}

void Skybox::drawSkyboxBack(){
	rt3d::drawIndexedMesh(SkyboxObjects,SkyboxIndexCount,GL_TRIANGLES);
}

void Skybox::drawSkyboxTop(){
	rt3d::drawIndexedMesh(SkyboxObjects,SkyboxIndexCount,GL_TRIANGLES);
}

void Skybox::drawSkyboxLeft(){
	rt3d::drawIndexedMesh(SkyboxObjects,SkyboxIndexCount,GL_TRIANGLES);
}

void Skybox::drawSkyboxRight(){
	rt3d::drawIndexedMesh(SkyboxObjects,SkyboxIndexCount,GL_TRIANGLES);

}
