#ifndef SKYBOX_H
#define SKYBOX_H


#include <SDL.h>
#include "md2model.h"
#include <GL/glew.h>
#include "rt3d.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>



class Skybox {
public:
	Skybox();
	~Skybox() { return; } // need a virtual destructor
	void init();
	void drawSkyboxFront();
	void drawSkyboxBack();
	void drawSkyboxTop();
	void drawSkyboxLeft();
	void drawSkyboxRight();
	GLuint GetSkyboxProgram(){return SkyboxProgram;}
	GLuint getSkytexture(int i) { return SkyboxTextures[i];}


private:
	GLuint SkyboxProgram;
	GLuint SkyboxObjects; 
	GLuint Skyboxtexture; 
	GLuint SkyboxVertCount;
	GLuint SkyboxIndexCount;
	GLuint SkyboxTextures [5];
	
};

#endif
