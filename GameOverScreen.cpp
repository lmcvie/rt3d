#include "GameOverScreen.h"
#include "game.h"

void GameOverScreen::init(Game& context) {
	label = new Label();
}

bool GameOverScreen::handleSDLEvent(SDL_Event const &sdlEvent, Game &context) {
	bool running = true;
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		if (sdlEvent.key.keysym.sym == SDLK_RETURN)
		{
			context.setState(context.getMainMenuState());
			running = true;
		}
		if (sdlEvent.key.keysym.sym == SDLK_ESCAPE)
		{
			running = true;
			context.setState(context.getMainMenuState());
		}
	return running;
}
}

void GameOverScreen::draw(SDL_Window* window, Game& context) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	label->textToTexture(textFont,"GAME     OVER!!!!");
	label->draw(-0.4,0.0);
	
	SDL_GL_SwapWindow(window);
	 
}