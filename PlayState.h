#ifndef PLAYSTATE_H
#define PLAYSTATE_H

#include <SDL.h>
#include "GameState.h"
#include "label.h"
#include "CombatScreen.h"
using namespace std;
class Game;


class PlayState: public GameState {
public:

	

	~PlayState() { return; } // need a virtual destructor
	void draw(SDL_Window * window, Game& context);
	//virtual void init(Game * context) = 0;
	void init(Game &context);
	// Not using update function yet
	// virtual void update(void) = 0
	bool handleSDLEvent(SDL_Event const &sdlEvent, Game &context);

private:
	float xpos, ypos, xsize, ysize;

	float FodderArrayXpos[5];
	float FodderArrayYpos[5];
	float BruteArrayXpos[3];
	float BruteArrayYpos[3];
	float raiderXpos;
	float raiderYpos;


	float healthpackXpos[2];
	float healthpackYpos[2];
	float combatpackXpos[2];
	float combatpackYpos[2];
	float stimulantXpos;
	float stimulantYpos;


	int combatpack, healthpack, stimulant, fullhealth;



	Label* label;
	Game* game;

	clock_t lastTime, currentTime;
	TTF_Font* textFont;	
};

#endif
