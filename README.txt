To run this project you will also need to install:
GLEW (Windows only)
SDL
SDL_ttf

For minimum fuss, using the supplied Visual Studio project files,
you can download the libraries from:
https://www.dropbox.com/sh/iri2z46jzoytcfg/j8bsqhdxw_

Save the folders into C:\dev 

So that you have the folders:
C:\dev\glew-1.7.0
C:\dev\SDL-2.0.0
C:\dev\SDL plugins


If you use a different path (or different versions of these libraries)
then you'll need to update the project properties to get things to compile.

The project uses the Maven Pro regular font created by Joe Prince and available
under the SIL Open Font License 1.1 - 
see http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL for more
information.