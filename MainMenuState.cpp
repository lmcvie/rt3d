#include "MainMenuState.h"
#include "game.h"
bool newgame = false;

void MainMenuState::init(Game& context) {
	label = new Label();
}

bool MainMenuState::handleSDLEvent(SDL_Event const &sdlEvent, Game &context) {
	bool running = true;
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		if (sdlEvent.key.keysym.sym == SDLK_c)
		{
			context.setState(context.getCombatScreen());
			running = true;
		}
		if (sdlEvent.key.keysym.sym == SDLK_ESCAPE)
		{
			running = false;
			SDL_DestroyWindow(context.window);
			SDL_Quit();
		}
		if (sdlEvent.key.keysym.sym == SDLK_r)
		{
			context.setState(context.getCreditScreen());
			running = true;
		}
		if (sdlEvent.key.keysym.sym == SDLK_n)
		{
			context.setState(context.getCharacterScreen());
			running = true;
			// set default values TO DO!!!!!!! from textfile
		}
	}
	return running;
}

void MainMenuState::draw(SDL_Window* window, Game& context) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	label->textToTexture(textFont, "Main Menu");
	label->draw(-0.1,0.4);
	label->textToTexture(textFont, "<N> New Game");
	label->draw(-0.15,0.2);
	label->textToTexture(textFont, "<Escape> Quit");
	label->draw(-0.15,-0.4);
	label->textToTexture(textFont, "<R> Credits");
	label->draw(-0.12,-0.2);
	label->textToTexture(textFont, "<C> Continue");
	label->draw(-0.15,0.0);
	SDL_GL_SwapWindow(window); 
}